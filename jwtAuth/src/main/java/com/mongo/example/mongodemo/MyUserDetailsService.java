package com.mongo.example.mongodemo;

import org.springframework.stereotype.Service;

//import com.mongo.example.mongodemo.models.apimodel.User;
import com.mongo.example.mongodemo.repository.UserDao;
import org.springframework.security.core.userdetails.User;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserDao userDao;

//	@Autowired
//	private PasswordEncoder bcryptEncoder;
	
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
    	com.mongo.example.mongodemo.models.apimodel.User user =  userDao.findUserByEmail(s);
//    	System.out.println("MyUserDetailsService user=>"+user);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + s);
		}
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				new ArrayList<>());
//    Optional<com.mongo.example.mongodemo.models.apimodel.User> user = userDao.findByUserName(s);
//    	 new CarUser(s);
//    return new CarUser(user);
    }
//    public com.mongo.example.mongodemo.models.apimodel.User save(com.mongo.example.mongodemo.models.apimodel.User user) {
//		com.mongo.example.mongodemo.models.apimodel.User newUser = new com.mongo.example.mongodemo.models.apimodel.User();
//		newUser.setUser_id(user.getUser_id());
//		newUser.setFirst_name(user.getFirst_name());
//		newUser.setLast_name(user.getLast_name());
//		newUser.setUsername(user.getUsername());
//		newUser.setEmail(user.getEmail());
//		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
//		 newUser.setRole(user.getRole());
//		newUser.setAddress(user.getAddress());
//		newUser.setImg(user.getImg());
//		newUser.setEnabled(user.isEnabled());
//		com.mongo.example.mongodemo.models.apimodel.User v = userDao.save(newUser);
//		return userDao.save(v);
//				
//	}
}