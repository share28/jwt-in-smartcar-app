package com.mongo.example.mongodemo.models.apimodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;


@Entity(name = "user")
@Table(name = "user",uniqueConstraints={@UniqueConstraint(columnNames ={"email"})})
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private int user_id;
	@Column(name = "first_name")
	private String first_name; 
	@Column(name = "last_name")
	private String last_name;
	private String username;
//	@Column(unique=true)
	private String email;   
	@NotNull
	private String password;
	private String role;
	private String address;   
	private String img;
	private boolean enabled;
	private String authToken;
	
	public User(int user_id) {
		super();
		this.user_id = user_id;
	}

	public User()
	{	
	}

	public User(int user_id, String first_name, String last_name, String username ,String email,
			String role, String address, String img, boolean enabled) {
			super();
			this.user_id = user_id;
			this.first_name = first_name;
			this.last_name = last_name;
			this.username= username;
			this.email = email;
			this.role = role;
			this.address = address;
			this.img = img;
			this.enabled = enabled;
		}

	public User(int user_id, String first_name, String last_name, String username ,String email, @NotNull String password,
		String role, String address, String img, boolean enabled) {
		super();
		this.user_id = user_id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.username= username;
		this.email = email;
		this.password = password;
		this.role = role;
		this.address = address;
		this.img = img;
		this.enabled = enabled;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
//	@Override
//	public String toString() {
//		return "User [user_id=" + user_id + ", first_name=" + first_name + ", last_name=" + last_name + ", email="
//				+ email + ", password=" + password + ", address=" + address + ", img=" + img + ", enabled=" + enabled
//				+ "]";
//	}

//	public Object getAuthorities() {
//		// TODO Auto-generated method stub
//		return null;
//	}
}