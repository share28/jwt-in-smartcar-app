package com.mongo.example.mongodemo;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;
import com.mongo.example.mongodemo.models.apimodel.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CarUser implements UserDetails {
	private static final long serialVersionUID = 1L;

//	private String email;
//	private String password;
	private boolean enabled;
//	private Collection<GrantedAuthority> authorities;
//	private String username;

User user =new User();
	
	public CarUser(User user) {
		super();
		this.user = user;
	}
	
//	public CarUser(User user) {
//		this.email = user.getEmail();
//		this.password = user.getPassword();
//		this.enabled = user.getEnabled() != 0;
//		this.authorities = user.getAuthorities()
//				.stream()
//				.map(role -> new SimpleGrantedAuthority()
//				.collect(Collectors.toList()));
//	}
	
//	public CarUser(String username) {
//		this.username =username;
//	}


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.singleton(new SimpleGrantedAuthority("user"));
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

//	@Override
//	public String getUsername() {
//		return user.getUsername();
//	}
	

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
}


