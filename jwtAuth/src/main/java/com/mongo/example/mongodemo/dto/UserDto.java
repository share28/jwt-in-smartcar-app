package com.mongo.example.mongodemo.dto;


public class UserDto {
	private String username;
	private String email;   
	private String address;   
	private String img;
	private String password;
	
	public UserDto() {
		super();
	}

	public UserDto(String username, String email, String address, String img,String password) {
		super();
		this.username = username;
		this.email = email;
		this.address = address;
		this.img = img;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserDto [user=" + username + ", email=" + email + ", address=" + address + ", img=" + img + ", password="
				+ password + "]";
	}


//	private User user;
//	private String email;   
//	private String address;   
//	private String img;
//	
//	public UserDto() {
//		super();
//	}
//
//	public UserDto(User user, String email, String address, String img) {
//		super();
//		this.user = user;
//		this.email = email;
//		this.address = address;
//		this.img = img;
//	}
//
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public String getImg() {
//		return img;
//	}
//
//	public void setImg(String img) {
//		this.img = img;
//	}
//
//	@Override
//	public String toString() {
//		return "UserDto [user=" + user + ", email=" + email + ", address=" + address + ", img=" + img + "]";
//	}
	
}