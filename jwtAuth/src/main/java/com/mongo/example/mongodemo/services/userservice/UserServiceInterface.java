package com.mongo.example.mongodemo.services.userservice;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.mongo.example.mongodemo.config.AuthenticationRequest;
import com.mongo.example.mongodemo.models.apimodel.User;

public interface UserServiceInterface {

	public List<User> getAllUsers();

	public User findUserById(int userId);

	public User findUserByEmail(String email);

	public User validateUser(User cred);

//	public User validateUser1(AuthenticationRequest cred);
	
	public User addnewUser(User user);

	public User getUserProfileById(int id);

	public User editProfile(int id, User user);

//	public ResponseEntity<String> authenticate(User cred);

//	ResponseEntity<String> authenticate(Credentials cred);
	
	
//	public ResponseEntity<?> createAuthenticationToken( AuthenticationRequest authenticationRequest) throws Exception ;

	
	
	
	
	
	
	
	
}
